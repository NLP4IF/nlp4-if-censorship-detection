# NLP4-IF--Censorship-Detection

TASK: Predict which tweet is going to be censored using only text.

# Censorship Dataset

The dataset contains censored and uncensored Sina Weibo posts collected for a period of 4 months (August 29, 2018 to December 29, 2018). To read more about the data collection, the reader is referred to the following paper:
- [Ng K., Feldman A., and J. Peng. Linguistic Fingerprints of Internet Censorship: The Case of Sina Weibo](https://aaai.org/ojs/index.php/AAAI/article/view/5381/5237)


Data format: 
tweet, label <0 = uncensored; 1 = censored>.


# Important Dates
- February 24, 2021: Training data released
- April 6, 2021: Test input released
- April 8, 2021: Test submissions due
- April 12, 2021: System descriptions due
- April 19, 2021: System descriptions notification of acceptance
- April 26, 2021: Camera-ready system descriptions due (hard deadline)

# Evaluation metrics
The official evaluation measure is F1, but we also report (Accuracy, Recall and Precision).
# Evaluation Results

NITK_NLP Team:


 precision    recall  f1-score   support

           0       0.61      0.73      0.66        91
           1       0.69      0.56      0.62        98
    accuracy                           0.64       189
    macro avg      0.65      0.64      0.64       189
    weighted avg   0.65      0.64      0.64       189




# Baseline
[Ng K., Feldman A., and J. Peng. Linguistic Fingerprints of Internet Censorship: The Case of Sina Weibo](https://aaai.org/ojs/index.php/AAAI/article/view/5381/5237)


# How to submit
The result file should be in the format of the training data, i.e., a tsv file, a tweet and its label.
The submissions should be done using this [form](https://forms.gle/euLDF5BaitwivcYV7).

# Licensing
These datasets are free for general research use.

# Credits

Kei Yin Ng

# Citation

@inproceedings{NLP4IF-2021-COVID19-task,
title = "Findings of the {NLP4IF}-2021 Shared Task on Fighting the {COVID}-19 Infodemic and Censorship Detection",
author = "Shaar, Shaden and
Firoj Alam and
Da San Martino, Giovanni and
Nikolov, Alex and
Zaghouani, Wajdi and
Nakov, Preslav and
Feldman, Anna",
booktitle = "Proceedings of the Fourth Workshop on Natural Language Processing for Internet Freedom: Censorship, Disinformation, and Propaganda",
series = {NLP4IF@NAACL'~21},
month = {June},
year = "2021",
address = "Online",
publisher = "Association for Computational Linguistics"
}

