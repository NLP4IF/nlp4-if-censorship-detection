
import logging
import os
from sklearn.metrics import accuracy_score, f1_score, precision_score, recall_score, classification_report, confusion_matrix, precision_recall_fscore_support
import pandas as pd
import sys
import argparse

"""
Compares the Submission file to the gold file
"""

logging.basicConfig(format='%(levelname)s : %(message)s', level=logging.INFO)

def _read_gold_and_pred(gold_fpath, pred_fpath):
    """
    Read gold and predicted data.
    :param gold_fpath: the original annotated gold file, .
    :param pred_fpath: a file with line_number and score at each line.
    :return: Label Dictionary.
    """

    logging.info("Reading gold predictions from file {}".format(gold_fpath))

    gold_path = gold_fpath
    Gold_df = pd.read_csv(gold_path, sep='\t', header=0)
    truths = Gold_df['label']


    logging.info('Reading Submission from file {}'.format(pred_fpath))
    
    Submission_path = pred_fpath
    Submission_df = pd.read_csv(Submission_path, sep='\t', header=0) 
    submitted = Submission_df["label"]


    ##This is meant to make sure the files are the same
    
    if len(Submission_df) != len(Gold_df):
        logging.error('The submissions do not match the lines from the gold file - missing or extra line in the file ')
        raise ValueError('The submissions do not match the lines from the gold file - missing or extra line in the file ')

    return truths, submitted

   
def evaluate(truths, submitted):


    ### if needed return A, F1, R, P, confusion matrix, etc. 
    #acc = accuracy_score(truths, submitted)
    #f1 = f1_score(truths, submitted, average='weighted')
    #p_score = precision_score(truths, submitted, average='weighted')
    #r_score = recall_score(truths, submitted, average='weighted')
    # confusionMatrix = confusion_matrix(truths, submitted)
   # return acc, f1, p_score, r_score, classificationReport, confusionMatrix

    classificationReport = classification_report(truths, submitted)
    return classificationReport
    

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--gold_file_path", "-g",
        help="Single string containing the path to file with gold standard.",
        type=str,
        required=True
    )
    parser.add_argument(
        "--pred_file_path","-p",
        help="Single string containing the path to file with submissions.",
        type=str,
        required=True
    )
    args = parser.parse_args()

    
    if _read_gold_and_pred(args.gold_file_path, args.pred_file_path):
        logging.info("Started evaluating results for the task ...")

        truths, submitted = _read_gold_and_pred(args.gold_file_path, args.pred_file_path)

        Print_Outputs = evaluate(truths, submitted)
        print(Print_Outputs)

    else:
        print('%sCOULDNT PROCESS THE FILE')


